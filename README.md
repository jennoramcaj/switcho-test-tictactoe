# Switcho Tic Tac Toe

### Start the project

First, of course, clone the repository. Then install composer dependencies

```bash
composer install
```

Run server on localhost

```bash
php -S localhost:8000 -t public
```

### Api

Install Postman and import the json file called `Switcho.postman_collection.json`. You can find it in the root project.

### Test

Execute test with a simple composer command

```bash
composer test
```
