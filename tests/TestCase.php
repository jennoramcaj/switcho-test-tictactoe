<?php

namespace Tests;

use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function startGame()
    {
        return $this->post('/start', [
            "first_player" => "Mario",
            "second_player" => "Giuseppe"
        ]);
    }

    public function startGameAndGetId()
    {
        $response = $this->startGame();

        return $response->response['game_id'];
    }
}
