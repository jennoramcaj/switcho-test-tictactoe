<?php

namespace Tests;

use App\Game;
use Illuminate\Support\Facades\Cache;

class GameTest extends TestCase
{
    public function test_ensure_cache_has_key_when_game_starts()
    {
        $response = $this->startGame();

        $gameId = $response->response['game_id'];

        $response->seeJsonContains([
            'game_id' => (string)$gameId
        ]);

        $this->assertTrue(Cache::has($gameId));
    }

    public function test_ensure_cache_has_correct_instance_in_cache()
    {
        $gameId = $this->startGameAndGetId();

        $this->assertContainsOnlyInstancesOf(
            Game::class,
            [Cache::get($gameId)]
        );
    }

    public function test_returns_error_when_game_id_does_not_exists()
    {
        $this->startGame();

        $this->post('/play', [
            "game_id" => "this_game_should_not_exists",
            "cell" => 9
        ])->seeJson([
            'error' => true,
            'message' => 'Game ID does not exists'
        ]);

        $this->get('/table/this_game_should_not_exists')->seeJson([
            'error' => true,
            'message' => 'Game ID does not exists'
        ]);
    }

    public function test_returns_error_when_game_id_is_not_set()
    {
        $this->startGame();

        $this->post('/play', [
            "cell" => 9
        ])->seeJson([
            'error' => true,
            'message' => 'Missing game ID parameter'
        ]);
    }

    public function test_cell_parameter_is_required()
    {
        $gameId = $this->startGameAndGetId();

        $this->post('/play', [
            'game_id' => $gameId
        ])->seeJson([
            'cell' => ['The cell field is required.']
        ]);
    }

    public function test_return_winner_name_after_game_completed()
    {
        $gameId = $this->startGameAndGetId();

        /**
         *   X | O |
         *  -----------
         *     | X | O
         *  -----------
         *     |   | X
         */

        // X player
        $this->post('/play', [
            'game_id' => $gameId,
            'cell' => 1
        ]);

        $this->post('/play', [
            'game_id' => $gameId,
            'cell' => 2
        ]);

        // X player
        $this->post('/play', [
            'game_id' => $gameId,
            'cell' => 5
        ]);

        $this->post('/play', [
            'game_id' => $gameId,
            'cell' => 6
        ]);

        // X player
        $this->post('/play', [
            'game_id' => $gameId,
            'cell' => 9
        ])->seeJson([
            'error' => false,
            'message' => 'Game completed',
            'winner' => 'Mario',
        ]);

    }
}
