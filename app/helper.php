<?php

use App\Game;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

if (!function_exists('gameInstance')) {
    function gameInstance(): Game
    {
        return Cache::get(Session::get('game_id'));
    }
}

if (!function_exists('players')) {
    function players()
    {
        return Cache::get(Session::get('game_id') . '_players');
    }
}
