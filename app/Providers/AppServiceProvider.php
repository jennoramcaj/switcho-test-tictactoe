<?php

namespace App\Providers;

use App\Game;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*$this->app->singleton(Game::class, function () {
            return Game::start();
        });*/
    }
}
