<?php

namespace App;

use App\Exceptions\CellNotAvailableException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class Game
{
    public array $table;
    private string $player = 'x';

    /**
     * @param $uid
     */
    public function __construct($uid)
    {
        $this->table = $this->createTable();
    }

    /**
     * @param $uid
     * @return static
     */
    public static function start($uid): static
    {
        return new static($uid);
    }

    /**
     * @throws \Throwable
     */
    public function play($cell): bool
    {
        switch ($cell) {
            case 1:
                $i = 0;
                $j = 0;
                break;
            case 2:
                $i = 0;
                $j = 1;
                break;
            case 3:
                $i = 0;
                $j = 2;
                break;
            case 4:
                $i = 1;
                $j = 0;
                break;
            case 5:
                $i = 1;
                $j = 1;
                break;
            case 6:
                $i = 1;
                $j = 2;
                break;
            case 7:
                $i = 2;
                $j = 0;
                break;
            case 8:
                $i = 2;
                $j = 1;
                break;
            case 9:
                $i = 2;
                $j = 2;
                break;
        }

        throw_if(!$this->isCellAvailable($i, $j), new CellNotAvailableException('Cell is not available', 400));

        $this->table[$i][$j] = $this->player;

        $this->setNextPlayer();

        $this->selfUpdate();

        return true;
    }

    public function isCompleted()
    {
        $winner = $this->winner();

        if ($winner !== null) {
            return [true, $winner];
        }

        // Check if at least one cell is null
        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 3; $j++) {
                if (is_null($this->table[$i][$j])) {
                    return [false, false];
                }
            }
        }

        return [true, null];
    }

    private function winner()
    {
        if ($this->checkHorizontal()) {
            return $this->checkHorizontal();
        }

        if ($this->checkVertical()) {
            return $this->checkVertical();
        }

        if ($this->checkDiagonal()) {
            return $this->checkDiagonal();
        }

        return null;
    }

    private function checkHorizontal()
    {
        for ($i = 0; $i < 3; $i++) {
            $winner = $this->table[$i][0];

            for ($j = 0; $j < 3; $j++) {
                if ($this->table[$i][$j] != $winner) {
                    $winner = null;
                    break;
                }
            }

            // Stop when we have a winner (because is not null)
            if ($winner !== null) {
                break;
            }
        }

        return $winner;
    }


    private function checkVertical()
    {
        for ($i = 0; $i < 3; $i++) {
            $winner = $this->table[0][$i];

            for ($j = 0; $j < 3; $j++) {
                if ($this->table[$j][$i] != $winner) {
                    $winner = null;
                    break;
                }
            }

            // Stop when we have a winner (because is not null)
            if ($winner !== null) {
                break;
            }
        }
        return $winner;
    }

    private function checkDiagonal()
    {
        // diagonal from left to right
        $winner = $this->table[0][0];
        for ($i = 0; $i < 3; $i++) {
            if ($this->table[$i][$i] != $winner) {
                $winner = null;
                break;
            }
        }

        // diagonal from right to left
        if ($winner === null) {
            $winner = $this->table[0][2];
            for ($i = 0; $i < 3; $i++) {
                if ($this->table[$i][2 - $i] != $winner) {
                    $winner = null;
                    break;
                }
            }
        }

        return $winner;
    }


    public function isCellAvailable($i, $j): bool
    {
        return is_null($this->table[$i][$j]);
    }

    public function setNextPlayer(): void
    {
        $this->player = $this->player === 'x' ? 'o' : 'x';
    }

    public function selfUpdate(): void
    {
        Cache::put(Session::get('game_id'), $this);
    }

    public function resetGame(): void
    {
        Cache::forget(Session::get('game_id'));
    }

    private function createTable(): array
    {
        $table = [];
        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 3; $j++) {
                $table[$i][$j] = null;
            }
        }
        return $table;
    }

}
