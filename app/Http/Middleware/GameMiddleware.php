<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class GameMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $gameId = $request->get('game_id') ?? $request->route('gameId');

        if (!$request->has('game_id') && !$request->route('gameId')) {
            return response()->json([
                'error' => true,
                'message' => __('Missing game ID parameter')
            ], 400);
        }

        if (!Cache::has($gameId)) {
            return response()->json([
                'error' => true,
                'message' => __('Game ID does not exists')
            ], 400);
        }

        Session::put('game_id', $gameId);

        return $next($request);
    }
}
