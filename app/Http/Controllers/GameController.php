<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class GameController extends Controller
{
    /**
     * Start a new Game
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function start(Request $request)
    {
        $this->validate($request, [
            'first_player' => 'required|string',
            'second_player' => 'required|string',
        ]);

        $gameId = uniqid();

        Cache::rememberForever($gameId, function () use ($gameId) {
           return Game::start($gameId);
        });

        Cache::rememberForever($gameId . '_players', function () use ($request) {
            return [
                'first_player' => $request->first_player,
                'second_player' => $request->second_player,
            ];
        });

        return response()->json([
            'game_id' => $gameId
        ]);
    }

    /**
     * Do a move
     *
     * @throws \Throwable
     */
    public function play(Request $request)
    {
        $this->validate($request, [
            'cell' => 'required|integer|min:1|max:9',
        ]);

        gameInstance()->play($request->cell);

        [$gameCompleted, $winner] = gameInstance()->isCompleted();
        if ($gameCompleted) {
            gameInstance()->resetGame();

            return response()->json([
                'error' => false,
                'message' => 'Game completed',
                'winner' => $this->getWinnerName($winner),
            ]);
        }

        return response()->json([
            'error' => false,
            'message' => 'ok'
        ]);
    }

    /**
     * Show current table
     *
     * @return JsonResponse
     */
    public function show()
    {
        return response()->json([
            'table' => gameInstance()->table
        ]);
    }

    /**
     * Retrieve winner name
     *
     * @param $winnerSymbol
     * @return mixed|string
     */
    private function getWinnerName($winnerSymbol)
    {
        if ($winnerSymbol == null) {
            return 'tie';
        }
        return $winnerSymbol == 'x' ? players()['first_player'] : players()['second_player'];
    }

}
